> Fill the build_config.properties
0. SET ALL PATHS TO YOU SDKS
1. FILE.APK - path to apk file with extension (uses only in android build)
2. FILE.IPA - path to ipa file with extension (uses only in ios build)
3. APP.NAME - name of the application (uses only in android build)
4. STORETYPE - by default PKCS12 if you use other change it (other types are not tested!) (uses only in android build)
5. STOREPASS - pass for you certificate (uses only in android build)
6. KEY_STORE - path to certificate (uses only in android build)
7. ALIAS_NAME - alias name for you certificate (uses only in android build) (LOOK REMOVE AIR PACKAGE)

> Remove air package from android/apk, you need to go to android folder.
1. If you don't know the alias name run "ant display_alias" and you can see what is you alias name by default is 1 but can be different
2. Run "ant doEverything" and if everything is ok and you've get BUILD SUCCESSFUL you should have final_*****.apk that you can use.
3. If something is wrong everything is splitted by many tasks and you can go step by step, everything is orderd how should go.
*If you want to do it yourself use http://helpx.adobe.com/air/kb/opt-out-air-application-analytics.html

> Remove metadata from ipa, you need to go to ios folder.
0. You should have antcontrib in ant/lib folder to use metadata remover
1. Run "ant doEverything" and if everything is ok and you've get BUILD SUCCESSFUL you should have final_*****.ios that you can use.
2. If something is wrong everything is splitted by many tasks and you can go step by step, everything is orderd how should go.
*If you want to do it yourself use http://helpx.adobe.com/air/kb/opt-out-air-app-analytics.html